package Contest;

abstract class TiketKonser implements HargaTiket {
    // Do your magic here...

    //Deklarasi variabel nama dan hargaTiket
    //dibawah ini digunakan untuk menyimpan data tiket konser.
    //2 variabel ini diberi aksesibilitas protected,
    //Maka dari itu, mereka dapat diakses oleh kelas turunan.
    protected String nama;
    protected double hargaTiket;

    //Konstruktor TiketKonser berfungsi untuk menginisialisasi
    //nilai nama dan hargaTiket ketika objek TiketKonser atau
    //objek turunannya dibuat.
    public TiketKonser(String nama, double hargaTiket) {
        this.nama = nama;
        this.hargaTiket = hargaTiket;
    }

    //Method getNama() dibawah ini berfungsi untuk mendapatkan informasi nama tiket konser.
    public String getNama() {
        return nama;
    }

    //Metode hitungHargaTiket() dideklarasikan sebagai
    //metode abstrak tanpa implementasi di kelas ini.
    //Metode ini bertanggung jawab untuk menghitung harga tiket
    //konser dan harus diimplementasikan di kelas turunan.
    public abstract double hitungHargaTiket();
}