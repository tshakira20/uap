package Contest;

class FESTIVAL extends TiketKonser {
    //Do your magic here...

    //Dibawah ini konstruktor kelas FESTIVAL menggunakan 2 parameter yaitu, nama dan hargaTiket.
    //Konstruktor ini memanggil konstruktor kelas induk TiketKonser menggunakan
    //kata kunci super dengan nilai nama dan hargaTiket yang diteruskan
    //ke konstruktor kelas induk.
    public FESTIVAL(String nama, double hargaTiket) {
        super(nama, hargaTiket);
    }

    //Dibawah ini merupakan penggunaan method hitungHargaTiket() yg dioverride dari kelas TiketKonser.
    //Method ini mengembalikan nilai hargaTiket, yang pada kelas ini merupakan harga tiket untuk
    //jenis FESTIVAL.
    @Override
    public double hitungHargaTiket() {
        return hargaTiket;
    }

}