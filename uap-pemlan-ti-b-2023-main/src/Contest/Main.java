/*
 * UAP PEMLAN 2023
 * DURASI: 120 MENIT
 * TEMPAT PENGERJAAN: DARING/LURING
 * =================================================================
 * Semangat mengerjakan UAP teman-teman
 * Jangan lupa berdoa untuk hasil yang terbaik
 */

package Contest;

import java.util.*;
import java.text.SimpleDateFormat;

public class Main {

    public static void main(String[] args) {
        //Do your magic here...
        Scanner scanner = new Scanner(System.in);

        System.out.println("Selamat datang di Pemesanan Tiket Coldplay!");

        try {
            System.out.print("Masukkan nama pemesan: ");
            String namaPemesan = scanner.nextLine();


            System.out.println("Pilih jenis tiket:");
            System.out.println("1. CAT 8");
            System.out.println("2. CAT 1");
            System.out.println("3. FESTIVAL");
            System.out.println("4. VIP");
            System.out.println("5. UNLIMITED EXPERIENCE");
            System.out.print("Masukkan pilihan: ");

            int choice;

            //Pada kode dibawah ini  Integer.parseInt berfungsi untuk mengubah
            //string menjadi interger. Kemudian hasil konversi disimpan dalam variabel choice.
            try {
                choice = Integer.parseInt(scanner.nextLine());
            }

            //Exception Handling dibawah ini berfungsi untuk penanganan kesalahan jika
            //input pengguna tidak valid, yaitu bukan angka dan diluar pilihan 1-5, dengan melemparkan
            //InvalidInputException dan memberikan pesan.
            catch (NumberFormatException e) {
                throw new InvalidInputException("Input tidak valid, Masukkan pilihan tiket sesuai angka yang tertera.");
            } if (choice < 1 || choice > 5) {
                throw new InvalidInputException("Input Anda tidak valid, Masukkan pilihan hanya angka 1-5");
            }



            //String dibawah ini di deklarasi untuk memanggil method generateKodeBooking()
            //untuk mendapatkan kode booking secara acak dan menyimpannya dalam variabel kodeBooking.
            String kodeBooking = generateKodeBooking();

            //String dibawah ini di deklarasi untuk memanggil method getCurrentDate()
            //untuk mendapatkan tanggal pesanan dan menyimpannya dalam variabel tanggalPesanan.
            String tanggalPesanan = getCurrentDate();

            // kode dibawah ini Menggunakan metode pilihTiket() dari kelas
            // PemesananTiket untuk memilih jenis tiket yang sesuai input user.
            // Kemudian disimpan dalam variabel tiket.
            TiketKonser tkt = PemesananTiket.pilihTiket(choice - 1);

            //Kode dibawah berfungsi untuk menghasilkan output dari program yang sesuai dengan input user
            System.out.println("\n----- Detail Pemesanan -----");
            System.out.println("Nama Pemesan: " + namaPemesan);
            System.out.println("Kode Booking: " + kodeBooking);
            System.out.println("Tanggal Pesanan: " + tanggalPesanan);
            System.out.println("Tiket yang dipesan: " + tkt.getNama());
            System.out.println("Total harga: " + tkt.hitungHargaTiket() + " USD");
        }

        //Kode dibawah ini berfungsi sebagai pesan ketika input user tidak sesuai
        catch (InvalidInputException e) {
            System.out.println("Terdapat kesalahan dari input anda: " + e.getMessage());
        }

        catch (Exception e) {
            System.out.println("Terdapat kesalahan dari input anda " + e.getMessage());
        }

        finally {
            scanner.close();
        }
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan kode pesanan atau kode booking
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String generateKodeBooking() {
        StringBuilder sb = new StringBuilder();
        String characters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
        int length = 8;

        for (int i = 0; i < length; i++) {
            int index = (int) (Math.random() * characters.length());
            sb.append(characters.charAt(index));
        }

        return sb.toString();
    }

    /*
     * Jangan ubah isi method dibawah ini, nama method boleh diubah
     * Method ini dipanggil untuk mendapatkan waktu terkini
     * Panggil method ini untuk kelengkapan mencetak output nota pesanan
     */
    public static String getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
        Date currentDate = new Date();
        return dateFormat.format(currentDate);
    }
}