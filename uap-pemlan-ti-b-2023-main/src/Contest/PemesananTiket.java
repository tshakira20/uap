package Contest;

class PemesananTiket {
    // Do your magic here...

    //Deklarasi variabel tiket dibawah ini merupakan array dari objek TiketKonser.
    //Array tiket berfungsi untuk menyimpan jenis tiket.
    public static TiketKonser[] tiket;

    //Pada kode dibawah ini 'static' digunakan untuk inisialisasi array tiket,
    //'static' berfungsi untuk memastikan array tersebut hanya diinisialisasi sekali
    //saat kelas PemesananTiket dijalankan. Pada kode dibawah Setiap objek tiket dibuat menggunakan konstruktor
    // kelas turunan dari TiketKonser (CAT8, CAT1, FESTIVAL, VIP, dan VVIP).
    static {
        tiket = new TiketKonser[]{
                new CAT8("CAT 8", 900000),
                new CAT1("CAT 1", 5000000),
                new FESTIVAL("FESTIVAL", 5700000),
                new VIP("VIP", 5000000),
                new VVIP("UNLIMITED EXPERIENCE", 11000000)
        };
    }

    //Method pilihTiket(int index) berfungsi untuk memilih dan mengembalikan
    //objek tiket dari array tiket berdasarkan indeks yang diberikan.
    //Indeks ini dilihat dari posisi tiket dalam array.
    public static TiketKonser pilihTiket(int index) {
        return tiket[index];
    }
}